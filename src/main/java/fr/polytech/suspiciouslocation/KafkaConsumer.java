package fr.polytech.suspiciouslocation;


import fr.polytech.suspiciouslocation.DTO.CredObjectFromKeycloak;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Component
class KafkaConsumer {
    private final WebClient webClient = WebClient.builder().build();
    private String token;

    private void requestToken() {
        MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();

        bodyValues.add("client_id", "suspicious-location-service");
        bodyValues.add("client_secret", "682f53b0-b3f6-48c6-b35d-ee90a3bd53a3");
        bodyValues.add("grant_type", "client_credentials");
        CredObjectFromKeycloak response = webClient.post()
                .uri("https://keycloak-covidalert.herokuapp.com/auth/realms/CovidAlert/protocol/openid-connect/token")
                .body(BodyInserters.fromFormData(bodyValues))
                .retrieve()
                .bodyToMono(CredObjectFromKeycloak.class)
                .block();
        this.token = response.getAccess_token();
    }

    @KafkaListener(topics = "UserLocation",
            groupId = "location")
    public void consume(Object loc)
    {
        System.out.println(loc.toString());
        /*
        requestToken();
        List response = webClient.get()
                .uri("http://localhost:8080/locations/square/latSW=11&longSW=11&latNE=14&longNE=14")
                .headers(header -> header.setBearerAuth(token))
                .retrieve()
                .bodyToMono(List.class)
                .block();
        System.out.println(response.toString());
        */

    }
}