package fr.polytech.suspiciouslocation;


import java.util.Date;


public class Location {

    public Location() {}

    public Location(Double latitude, Double longitude, Date date, String id_user) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.id_user = id_user;
    }

    public Location(long id_location, Double latitude, Double longitude, Date date, String id_user) {
        this.id_location = id_location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.id_user = id_user;
    }

    private long id_location;

    private Double latitude;
    private Double longitude;
    private Date date;
    private String id_user;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String user) {
        this.id_user = user;
    }

    public long getId_location() {
        return id_location;
    }

    public void setId_location(long id_location) {
        this.id_location = id_location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
